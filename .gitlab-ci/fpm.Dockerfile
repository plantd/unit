# Base image: https://hub.docker.com/_/golang/
# create: "docker build -t registry.gitlab.com/plantd/unit:fpm -f .gitlab-ci/fpm.Dockerfile ."
FROM golang:1.13.15-buster

RUN apt-get update \
    && apt-get install -y --no-install-recommends\
        build-essential \
        apt-utils \
        git \
        ruby \
        ruby-dev \
        libzmq5 \
        libzmq3-dev \
        libczmq4 \
        libczmq-dev \
    && gem install --no-ri --no-rdoc fpm
