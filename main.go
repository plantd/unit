package main

import (
	"gitlab.com/plantd/unit/cmd"
)

func main() {
	cmd.Execute()
}
