PROJECT=unit
REGISTRY=registry.gitlab.com/plantd/$(PROJECT)
PREFIX ?= /usr
DESTDIR ?=
CONFDIR = /etc
SYSTEMDDIR = /lib/systemd

M := $(shell printf "\033[34;1m▶\033[0m")
TAG := $(shell git describe --all | sed -e's/.*\///g')
VERSION := $(shell git describe)
BINARY_NAME := "plantd-unit"

all: build

lint: ; $(info $M Linting the files)
	@./tools/checks lint

test: ; $(info $M Running unittests)
	@./tools/checks test

race: ; $(info $M Running data race detector)
	@./tools/checks race

msan: ; $(info $M Running memory sanitizer)
	@./tools/checks msan

coverage: ; $(info $M Generating global code coverage report)
	@./tools/coverage

coverhtml: ; $(info $M Generating global code coverage report in HTML)
	@./tools/coverage html

build: ; $(info $(M) Building plantd project...)
	@go build -a -o target/$(BINARY_NAME) -ldflags "-X gitlab.com/plantd/unit/pkg.VERSION=$(VERSION)"

image: ; $(info $(M) Building application image...)
	@docker build -t $(REGISTRY) -f ./build/Dockerfile .

container: image ; $(info $(M) Running application container...)
	@docker run -p 7201-7205:7201-7205 $(REGISTRY):latest

install: ; $(info $(M) Installing plantd unit services...)
	@install -Dm 755 target/$(BINARY_NAME) "$(DESTDIR)$(PREFIX)/bin/plantd-unit"
	@install -Dm 644 README.md "$(DESTDIR)$(PREFIX)/share/doc/plantd/unit/README"
	@install -Dm 644 LICENSE "$(DESTDIR)$(PREFIX)/share/licenses/plantd/unit/COPYING"
	@install -d "$(DESTDIR)$(CONFDIR)/plantd/unit/"
	@install -Dm 644 configs/*.yaml "$(DESTDIR)$(CONFDIR)/plantd/unit/"
	@install -Dm 644 init/plantd-unit@.service "$(DESTDIR)$(SYSTEMDDIR)/system/plantd-unit@.service"

uninstall: ; $(info $(M) Uninstalling plantd unit services...)
	@rm $(DESTDIR)$(PREFIX)/target/$(BINARY_NAME)

clean: ; $(info $(M) Removing generated files... )
	@rm -rf target/

.PHONY: all lint test race msan coverage coverhtml build build-deb static install uninstall clean
