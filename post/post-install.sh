#!/bin/bash

systemctl start plantd-unit@acquire.service
systemctl start plantd-unit@analyze.service
systemctl start plantd-unit@control.service
systemctl start plantd-unit@record.service
systemctl start plantd-unit@state.service
