[![Build Status](https://gitlab.com/plantd/unit/badges/master/build.svg)](https://gitlab.com/plantd/unit/commits/master)
[![Coverage Report](https://gitlab.com/plantd/unit/badges/master/coverage.svg)](https://gitlab.com/plantd/unit/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/plantd/unit)](https://goreportcard.com/report/gitlab.com/plantd/unit)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

---

# Plant Services

Beta state, API subject to breaking changes. In general this service is
deprecated and will likely be removed in the future.

## Setup

### Dependencies

Installation of these is independent of the distribution used.

* go
* ZeroMQ

### Plantd Dependencies

* plantd-master
* plantd-configure
* plantd-broker

### Build

```sh
make && sudo make install
```

or using Docker

```sh
make image
```

## Running

The application can either be run from the resulting `target/` directory, or the
`systemd` service file can be used.

For example, to use the `acquire` unit:

```sh
sudo systemctl enable plantd-unit@acquire
sudo systemctl start plantd-unit@acquire
```

### Environment

In general every configuration variable can be overridden by an environment
variable. For example, the configuration section:

```yaml
log:
  debug: false
  formatter: "json"
  level: "info"
```

is equivalent to:

```sh
PLANTD_UNIT_LOG_DEBUG=false
PLANTD_UNIT_LOG_FORMATTER=json
PLANTD_UNIT_LOG_LEVEL=info
```

The exception to this is any of the configuration sections for `buses` and
`modules`, those are mapped by name at runtime and do not have variables
created for them.

An additional environment variable is available to set the configuration file
using `PLANTD_UNIT_CONFIG`. This file needs to end in one of: `yaml`, `json`,
`hcl`, `toml`, or `conf`.

## Publishing

```sh
docker build -t registry.gitlab.com/plantd/unit:v1 -f ./build/Dockerfile .
docker push registry.gitlab.com/plantd/unit:v1
```
