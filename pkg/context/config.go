package context

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type broker struct {
	Endpoint string `mapstructure:"endpoint"`
}

type unit struct {
	Endpoint          string `mapstructure:"endpoint"`
	HeartbeatLiveness int    `mapstructure:"heartbeat-liveness"`
	HeartbeatInterval int    `mapstructure:"heartbeat-interval"`
}

type log struct {
	Debug     bool   `mapstructure:"debug"`
	Formatter string `mapstructure:"formatter"`
	Level     string `mapstructure:"level"`
}

type bus struct {
	Frontend string `mapstructure:"frontend"`
	Backend  string `mapstructure:"backend"`
	Capture  string `mapstructure:"capture"`
}

type module struct {
	Endpoint    string `mapstructure:"endpoint"`
	Description string `mapstructure:"description"`
}

type Config struct {
	App     string            `mapstructure:"app"`
	Broker  broker            `mapstructure:"broker"`
	Unit    unit              `mapstructure:"unit"`
	Log     log               `mapstructure:"log"`
	Buses   map[string]bus    `mapstructure:"buses"`
	Modules map[string]module `mapstructure:"modules"`
}

func LoadConfig(path, name string) (*Config, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()

	file := os.Getenv("PLANTD_UNIT_CONFIG")
	if file == "" {
		config.SetConfigName(name)
		config.AddConfigPath(".")
		config.AddConfigPath(path)
		config.AddConfigPath(fmt.Sprintf("%s/.config/plantd/unit", home))
		config.AddConfigPath("/etc/plantd/unit")
	} else {
		base := filepath.Base(file)
		if strings.HasSuffix(base, "yaml") ||
			strings.HasSuffix(base, "json") ||
			strings.HasSuffix(base, "hcl") ||
			strings.HasSuffix(base, "toml") ||
			strings.HasSuffix(base, "conf") {
			// strip the file type for viper
			parts := strings.Split(filepath.Base(file), ".")
			base = strings.Join(parts[:len(parts)-1], ".")
		}
		config.SetConfigName(base)
		config.AddConfigPath(filepath.Dir(file))
	}

	err = config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	config.SetEnvPrefix("PLANTD_UNIT")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	var c Config

	err = config.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	// initialize logging
	switch c.Log.Formatter {
	case "json":
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}

	level, err := logrus.ParseLevel(c.Log.Level)
	if err == nil {
		logrus.SetLevel(level)
	}

	return &c, nil
}
