package service

import (
	"gitlab.com/plantd/unit/pkg/context"

	"gitlab.com/plantd/broker/pkg/mdp"
)

// TODO: use this or get rid of it

type Endpoint struct {
	name   string
	client *mdp.Client
	config *context.Config
}

func NewEndpoint(name string, client *mdp.Client, config *context.Config) *Endpoint {
	return &Endpoint{
		name,
		client,
		config,
	}
}
