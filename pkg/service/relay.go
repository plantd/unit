package service

import (
	"fmt"

	"gitlab.com/plantd/unit/pkg/context"
	"gitlab.com/plantd/unit/pkg/worker"

	"gitlab.com/plantd/broker/pkg/bus"
	"gitlab.com/plantd/broker/pkg/mdp"

	"github.com/sirupsen/logrus"
)

type Relay struct {
	broker   string
	endpoint string
	service  string
	buses    map[string]*bus.Bus
	modules  map[string]*mdp.Client
}

func NewRelay(config *context.Config) *Relay {
	var buses map[string]*bus.Bus = make(map[string]*bus.Bus)
	var modules map[string]*mdp.Client = make(map[string]*mdp.Client)

	for name, b := range config.Buses {
		logrus.WithFields(logrus.Fields{
			"bus":      name,
			"frontend": b.Frontend,
			"backend":  b.Backend,
			"capture":  b.Capture,
		}).Info("adding message bus")
		buses[name] = bus.NewBus(name, config.App, b.Frontend, b.Backend, b.Capture)
	}

	for name, module := range config.Modules {
		var err error
		logrus.WithFields(logrus.Fields{
			"module":      name,
			"description": module.Description,
			"endpoint":    module.Endpoint,
		}).Info("adding unit module")
		modules[name], err = mdp.NewClient(module.Endpoint)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"module": name,
			}).Errorf("failed to create mdp client: %s", err)
		}
	}

	return &Relay{
		broker:   config.Broker.Endpoint,
		endpoint: config.Unit.Endpoint,
		service:  config.App,
		buses:    buses,
		modules:  modules,
	}
}

func (r *Relay) Run(done chan bool) {
	doneBuses := make(map[string](chan bool))
	doneModules := make(map[string](chan bool))
	moduleRequests := make(map[string](chan []byte))
	moduleResponses := make(map[string](chan []byte))

	for name, bus := range r.buses {
		doneBuses[name] = make(chan bool, 1)
		go bus.Run(doneBuses[name])
	}

	for name, module := range r.modules {
		doneModules[name] = make(chan bool, 1)
		moduleRequests[name] = make(chan []byte, 2)
		moduleResponses[name] = make(chan []byte, 2)
		go r.plantClient(module, name, doneModules[name], moduleRequests[name], moduleResponses[name])
	}

	handler := worker.NewHandler(moduleRequests, moduleResponses)
	w, err := worker.NewWorker(r.broker, r.service, handler)
	if err != nil {
		logrus.Error(err)
		done <- true
	}

	go w.Run()
	if err != nil {
		logrus.Error(err)
		done <- true
	}

	// FIXME: nothing waits for doneModules[] channels

	done <- true
}

// This client connects to the broker service
//
// TODO: pass in a service statistics channel to record eg. error count, MPS
// FIXME: the client is already available at r.modules[name]
func (r *Relay) plantClient(client *mdp.Client, name string, done chan bool, req <-chan []byte, resp chan<- []byte) {
	for {
		// Get message from the request channel
		msg := <-req
		body := <-req

		logrus.WithFields(logrus.Fields{
			"module": name,
			"data":   string(msg),
		}).Debug("received message on channel")

		request := make([]string, 2)
		request[0] = string(msg)
		request[1] = string(body)

		// Send it to the module using the plant client
		err := client.Send(name, request...)
		if err != nil {
			logrus.Errorf("error during send: %s", err)
			continue
		}

		// Wait for the module's response
		response, err := client.Recv()
		if err != nil {
			// XXX: consider filling actual response type here
			logrus.WithFields(logrus.Fields{
				"request":  request,
				"response": response,
			}).Errorf("error during response", err)
			logrus.Errorf("error during receive: %s", err)
			resp <- []byte("error")
			re := fmt.Sprintf("%s", err)
			resp <- []byte(re)
			continue
		}

		logrus.Debugf("response size: %d", len(response))
		logrus.Debugf("response[0]: %s", response[0])
		logrus.Debugf("response[1]: %s", response[1])

		// Send it back to the message handler
		// XXX: this should handle all responses, not just the first
		resp <- []byte(response[0])
		resp <- []byte(response[1])
		//resp <- []byte(response[2])
	}

	done <- true
}
