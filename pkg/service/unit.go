package service

import (
	"context"

	gcontext "gitlab.com/plantd/unit/pkg/context"

	"gitlab.com/plantd/broker/pkg/mdp"

	"github.com/sirupsen/logrus"
)

type keyUnitContext int

const (
	keyConfig keyUnitContext = iota + 1
	keyBroker
	keyRelay
)

type Unit struct {
	name   string
	config *gcontext.Config
	broker *mdp.Broker
	relay  *Relay
}

func NewUnit(name string) (*Unit, error) {
	config, err := gcontext.LoadConfig("configs", name)
	if err != nil {
		return nil, err
	}

	ctx := context.Background()

	broker, _ := mdp.NewBroker(config.Unit.Endpoint)
	broker.Bind()

	relay := NewRelay(config)

	// Add services to context
	ctx = context.WithValue(ctx, keyConfig, config)
	ctx = context.WithValue(ctx, keyBroker, broker)
	ctx = context.WithValue(ctx, keyRelay, relay)

	return &Unit{
		name:   name,
		config: config,
		broker: broker,
		relay:  relay,
	}, nil
}

func (u *Unit) Start() {
	doneBroker := make(chan bool, 1)
	doneRelay := make(chan bool, 1)

	go u.broker.Run(doneBroker)
	go u.relay.Run(doneRelay)

	// TODO: send exit to client if service quit received

	<-doneBroker
	<-doneRelay

	logrus.Info("unit shutting down")
}
