package model

import (
	"encoding/json"
	"fmt"
)

type RequestMessage struct {
	Name string      `json:"msg-type"`
	Body interface{} `json:"msg-body,omitempty"`
}

type ResponseMessage struct {
	Name string      `json:msg-type`
	Body interface{} `json:"msg-body,omitempty"`
}

type StatusResponseBody struct {
	Status string `json:"status"`
}

type Unpacker struct {
	Data interface{}
}

// General process:
//
//  - unmarshal
//  - check that something was unmarshaled, if yes return
//  - check for an error other than type, if yes return
func (u *Unpacker) UnmarshalJSON(b []byte) error {
	// RequestMessage
	reqMsg := &RequestMessage{}
	err := json.Unmarshal(b, reqMsg)

	if err == nil && reqMsg.Name != "" {
		u.Data = reqMsg
		return nil
	}

	if _, ok := err.(*json.UnmarshalTypeError); err != nil && !ok {
		return err
	}

	// ResponseMessage
	respMsg := &ResponseMessage{}
	err = json.Unmarshal(b, respMsg)

	if err == nil && respMsg.Name != "" {
		u.Data = respMsg
		return nil
	}

	if _, ok := err.(*json.UnmarshalTypeError); err != nil && !ok {
		return err
	}

	// Couldn't resolve type
	return nil
}

func (u *Unpacker) String() string {
	switch d := u.Data.(type) {
	case *RequestMessage:
		return fmt.Sprint("Request: ", d)
	case *ResponseMessage:
		return fmt.Sprint("Response: ", d)
	}
	return ""
}
