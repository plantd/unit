package worker

import (
	"bytes"
	"fmt"

	pb "gitlab.com/plantd/broker/pkg/proto/v1"

	"github.com/golang/protobuf/jsonpb"
	"github.com/sirupsen/logrus"
)

type Handler struct {
	/*
	 *service   *service.Service
	 */
	requests  map[string](chan []byte)
	responses map[string](chan []byte)
}

func NewHandler(requests map[string](chan []byte), responses map[string](chan []byte)) *Handler {
	return &Handler{
		requests:  requests,
		responses: responses,
	}
}

// TODO: not implemented, just stubbed
func (h *Handler) GetConfiguration(body string) ([]byte, error) {
	// XXX: currently using request used with configure service, this could be empty
	request := &pb.ConfigurationRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request with real data

	// TODO: use service config
	configuration := &pb.Configuration{}
	response := &pb.ConfigurationResponse{
		Configuration: configuration,
	}

	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetStatus(StatusRequest) returns (StatusResponse);
func (h *Handler) GetStatus(body string) ([]byte, error) {
	request := &pb.StatusRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request with real data

	// XXX: don't actually care about the body in this one if it's a pb.Empty
	details := make(map[string]string)
	details["test-key"] = "test-value"

	// TODO: get actual status response
	status := &pb.Status{
		Enabled: true,
		Loaded:  true,
		Active:  true,
		Details: details,
	}
	response := &pb.StatusResponse{
		Status: status,
	}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetSettings(SettingsRequest) returns (SettingsResponse);
func (h *Handler) GetSettings(body string) ([]byte, error) {
	// SettingsRequest is empty
	request := &pb.SettingsRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	settings := make(map[string]string)
	settings["test-key"] = "test-value"

	response := &pb.SettingsResponse{
		Id:       "1234",
		Settings: settings,
	}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetChannel(ChannelRequest) returns (ChannelResponse);
func (h *Handler) GetChannel(body string) ([]byte, error) {
	request := &pb.ChannelRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.ChannelResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetChannels(Empty) returns (ChannelsResponse);
func (h *Handler) GetChannels(body string) ([]byte, error) {
	request := &pb.Empty{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.ChannelsResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetJob(JobRequest) returns (JobResponse);
func (h *Handler) GetJob(body string) ([]byte, error) {
	request := &pb.JobRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetJobs(Empty) returns (JobsResponse);
func (h *Handler) GetJobs(body string) ([]byte, error) {
	request := &pb.Empty{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobsResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetJobStatus(JobRequest) returns (JobStatusResponse);
func (h *Handler) GetJobStatus(body string) ([]byte, error) {
	request := &pb.JobRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobStatusResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetModule(ModuleRequest) returns (ModuleResponse);
func (h *Handler) GetModule(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.ModuleResponse{
		Module: &pb.Module{
			Id:          "acquire-genicam",
			Name:        "genicam",
			ServiceName: "acquire",
		},
	}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetModules(Empty) returns (ModulesResponse);
func (h *Handler) GetModules(body string) ([]byte, error) {
	request := &pb.Empty{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.ModulesResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (h *Handler) GetModuleConfiguration(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle get-module-configuration: %s", body)
	h.requests[module] <- []byte("get-configuration")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-configuration", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received configuration response: %s", string(response))

	return response, nil
}

func (h *Handler) GetModuleStatus(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle get-module-status: %s", body)
	h.requests[module] <- []byte("get-status")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-status", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received status response: %s", string(response))

	return response, nil
}

func (h *Handler) GetModuleSettings(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle get-module-settings: %s", body)
	h.requests[module] <- []byte("get-settings")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-settings", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received settings response: %s", string(response))

	return response, nil
}

func (h *Handler) GetModuleJob(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle get-module-job", body)
	h.requests[module] <- []byte("get-job")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-job", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received job response: %s", string(response))

	return response, nil
}

func (h *Handler) GetModuleJobs(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle get-module-jobs: %s", body)
	h.requests[module] <- []byte("get-jobs")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-jobs", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received jobs response: %s", string(response))

	return response, nil
}

func (h *Handler) GetModuleActiveJob(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle get-module-active-job: %s", body)
	h.requests[module] <- []byte("get-active-job")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-active-job", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received job response response: %s", string(response))

	return response, nil
}

func (h *Handler) ModuleCancelJob(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle module-cancel-job: %s", body)
	h.requests[module] <- []byte("cancel-job")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "module-cancel-job", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received job response: %s", string(response))

	return response, nil
}

func (h *Handler) ModuleSubmitJob(body string) ([]byte, error) {
	request := &pb.ModuleJobRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle module-submit-job: %s", body)
	h.requests[module] <- []byte("submit-job")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "module-submit-job", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received job response: %s", string(response))

	return response, nil
}

func (h *Handler) ModuleSubmitEvent(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle module-submit-event: %s", body)
	h.requests[module] <- []byte("submit-event")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "module-submit-event", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received job response: %s", string(response))

	return response, nil
}

func (h *Handler) ModuleAvailableEvents(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle module-available-events: %s", body)
	h.requests[module] <- []byte("available-events")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "module-available-events", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received events response: %s", string(response))

	return response, nil
}

func (h *Handler) GetModuleProperty(body string) ([]byte, error) {
	request := &pb.PropertyRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle get-module-property: %s", body)
	h.requests[module] <- []byte("get-property")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-property", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received property response: %s", string(response))

	return response, nil
}

func (h *Handler) SetModuleProperty(body string) ([]byte, error) {
	request := &pb.PropertyRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle set-module-property: %s", body)
	h.requests[module] <- []byte("set-property")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "set-module-property", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received property response: %s", string(response))

	return response, nil
}

func (h *Handler) GetModuleProperties(body string) ([]byte, error) {
	request := &pb.PropertiesRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle get-module-properties: %s", body)
	h.requests[module] <- []byte("get-properties")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-properties", string(eh))
		resp := &pb.PropertiesResponse{
			Error: &pb.Error{Code: 501, Message: string(eh)},
		}
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err := marshaler.Marshal(&b, resp); err != nil {
			logrus.Errorf("jsonpb marshal: %s", err)
			// XXX: same issue not returning something here, but this really shouldn't happen
			return nil, err
		}
		return b.Bytes(), re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received properties response: %s", string(response))

	return response, nil
}

func (h *Handler) SetModuleProperties(body string) ([]byte, error) {
	request := &pb.PropertiesRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle set-module-properties: %s", body)
	h.requests[module] <- []byte("set-properties")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "set-module-properties", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received property response: %s", string(response))

	return response, nil
}

func (h *Handler) ModuleShutdown(module, body string) ([]byte, error) {
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("handle module-shutdown: %s", body)
	h.requests[module] <- []byte("shutdown")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "module-shutdown", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	logrus.WithFields(logrus.Fields{"module": module}).Debugf("received events response: %s", string(response))

	return response, nil
}
