package worker

import (
	"errors"

	"gitlab.com/plantd/broker/pkg/mdp"

	"github.com/sirupsen/logrus"
)

//  The worker class defines a single worker instance:
type Worker struct {
	session  *mdp.Worker //  MDP worker session
	endpoint string      //  Broker binds to this endpoint
	handler  *Handler
}

func NewWorker(endpoint string, name string, handler *Handler) (worker *Worker, err error) {
	session, err := mdp.NewWorker(endpoint, name)
	if err != nil {
		return nil, err
	}

	//  Initialize broker state
	worker = &Worker{
		session:  session,
		endpoint: endpoint,
		handler:  handler,
	}

	return worker, nil
}

func (w *Worker) Run() error {
	var err error
	var request, reply []string
	for {
		request, err = w.session.Recv(reply)
		if err != nil {
			return err
		}

		if len(request) == 0 {
			return errors.New("request has zero parts")
		}

		msgType := request[0]

		// Reset reply
		reply = []string{}
		multiPart := false

		for _, part := range request[1:] {
			var data []byte
			// Note: the rule for logging is:
			// status-request: debug level
			// actions: info level
			switch msgType {
			case "get-configuration":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetConfiguration(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-status":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetStatus(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-settings":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetSettings(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-channel":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetChannel(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-channels":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetChannels(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-job":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetJob(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-jobs":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetJobs(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-job-status":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetJobStatus(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-module":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetModule(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-modules":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetModules(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-module-configuration":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetModuleConfiguration(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-module-status":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetModuleStatus(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-module-settings":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetModuleSettings(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-module-job":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetModuleJob(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-module-jobs":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetModuleJobs(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-module-active-job":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetModuleActiveJob(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "module-cancel-job":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Infof(msgType)
				if data, err = w.handler.ModuleCancelJob(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "module-submit-job":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Infof(msgType)
				if data, err = w.handler.ModuleSubmitJob(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "module-submit-event":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Infof(msgType)
				if data, err = w.handler.ModuleSubmitEvent(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "module-available-events":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.ModuleAvailableEvents(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-module-property":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetModuleProperty(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "set-module-property":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Infof(msgType)
				if data, err = w.handler.SetModuleProperty(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "get-module-properties":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Debugf(msgType)
				if data, err = w.handler.GetModuleProperties(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				//break
			case "set-module-properties":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Infof(msgType)
				if data, err = w.handler.SetModuleProperties(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
				break
			case "module-shutdown":
				logrus.WithFields(logrus.Fields{
					"details": part,
				}).Infof(msgType)
				if !multiPart {
					if data, err = w.handler.ModuleShutdown(part, "shutdown"); err != nil {
						logrus.WithFields(logrus.Fields{
							"type": msgType,
						}).Warnf("message failed: %s", err)
						break
					}
				}
				multiPart = true
				break
			default:
				// TODO:
				logrus.Errorf("invalid message type provided: %s", msgType)
				break
			}

			// Append
			reply = append(reply, string(data))
			logrus.Debug(reply)
		}
	}

	return nil
}
